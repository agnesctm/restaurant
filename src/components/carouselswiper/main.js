// Import Swiper Vue.js components
import { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/vue';

  // Import Swiper styles
import 'swiper/css';

  export default {
    name: "CarouselSwiper",
    components: {
      Swiper,
      SwiperSlide,
    },
    setup() {
      const onSwiper = (swiper) => {
        window.swiper = swiper;
      };
      return {
        modules: [Navigation, Pagination, Scrollbar, A11y],
        onSwiper,
      };
    },
  };