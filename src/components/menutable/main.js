export default {
  name: "MenuTable",
  data() {
    return {
      beefs: [
        {
          item: "Burger",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$20"
        },
        {
          item: "Pasta",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        },
        {
          item: "Noodles",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        },
        {
          item: "Bread",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        }
      ],
      chickens: [
        {
          item: "Burger",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$20"
        },
        {
          item: "Pasta",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        },
        {
          item: "Burger",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$20"
        },
        {
          item: "Pasta",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        },
        {
          item: "Noodles",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        },
        {
          item: "Bread",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        }
      ],
      seafoods: [
        {
          item: "Pasta",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        },
        {
          item: "Burger",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$20"
        },
        {
          item: "Pasta",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        },
        {
          item: "Noodles",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        },
        {
          item: "Bread",
          description: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit",
          price: "$100"
        }
      ],
    };
  },
};