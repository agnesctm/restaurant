import MenuTable from '../menutable/MenuTable.vue'
import CardGrid from '../cardgrid/CardGrid.vue'

export default {
  name: "AccordionMenu",
  components: {
    CardGrid,
    MenuTable
  },
  data() {
    return {
      contents: [
        {
          id: "collapseOne",
          img: "https://images.pexels.com/photos/1640777/pexels-photo-1640777.jpeg",
          title: "breakfast",
          description: "description"
        },
        {
          id: "collapseTwo",
          img: "https://images.pexels.com/photos/1640777/pexels-photo-1640777.jpeg",
          title: "breakfast",
          description: "description"
        }
      ],
    };
  },
};
