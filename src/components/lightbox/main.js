
import Lightbox from 'bs5-lightbox'

import VueEasyLightbox from 'vue-easy-lightbox'

export default {
    name: "LightBox",
    components: {
      Lightbox,
      VueEasyLightbox
    },
    data() {
      return {
        contents: [
          {
            img: "https://unsplash.it/600.jpg?image=251"
          },
          {
            img: "https://unsplash.it/600.jpg?image=252"
          },
          {
            img: "https://unsplash.it/600.jpg?image=253"
          },
          {
            img: "https://unsplash.it/600.jpg?image=254"
          },
          {
            img: "https://unsplash.it/600.jpg?image=255"
          },
          {
            img: "https://unsplash.it/600.jpg?image=256"
          }
        ],
        imgs: 'https://unsplash.it/600.jpg?image=251',
        visible: false,
        index: 0
      }
    },
    methods: {
      showLightBox() {

        this.index = 1
        this.show()
      },
      show() {
        this.visible = true
      },
      handleHide() {
        this.visible = false
      }
    }
  };

document.querySelectorAll('.my-lightbox-toggle').forEach((el) => el.addEventListener('click', (e) => {
	e.preventDefault();
	const lightbox = new Lightbox(el);
	lightbox.show();
}));
